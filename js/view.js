/**
 * Publicly bindable events
 */
mapReadyEvent       = () => console.log('map ready');
markerSelectedEvent = (model) => console.log(`marker ${model._id} selected`);

let _map;
let _markers = Object();
let _polylines = Object();

function initializeMap(canvasId) {
  var lisboaFirCenter = new google.maps.LatLng(38.846122, -10.0676154);
  var mapOptions = {
    zoom: 3,
    gestureHandling: 'greedy',
    disableDefaultUI: true,
    maxZoom:16,
    center: lisboaFirCenter,
    geodesic: true,
    mapTypeId: 'satellite',
  };
  _map = new google.maps.Map(document.getElementById(canvasId), mapOptions);
  google.maps.event.addDomListener(window, 'load', mapReadyEvent);
}

function renderMarkers(models) {
  // remove rendered markers
  var modelIds = new Set(models.map(e => e._id));
  Object.entries(_markers).forEach(function ([id, marker]) {
    if (!modelIds.has(id)) {
      _removeMarker(marker);
      _markers[id] = undefined;
    }
  });

  // render markers
  models.forEach(function (model) {
    let marker = _markers[model._id];

    if (marker) {
      _updateMarker(marker, model);
    } else {
      _markers[model._id] = _addMarker(model);
    }
  });
}

function renderPolylines(models) {
  // remove rendered polylines
  var modelIds = new Set(models.map(e => e._id));
  Object.entries(_polylines).forEach(function ([id, polyline]) {
    if (!modelIds.has(id)) {
      _removePolyline(polyline);
      _polylines[id] = undefined;
    }
  });

  // render polylines
  models.forEach(function (model) {
    let polyline = _polylines[model._id];

    if (!polyline) {
      _polylines[model._id] = _addPolyline(model);
    }
  });
}

function _addMarker(model) {
  var color = model.selected ? 'red' : 'yellow' || 'yellow';

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(...model.position),
    title   : model.title,
    icon    : {
      url   : `img/aircraft-${color}-icons.png`,
      size  : new google.maps.Size(...model.icon.size),
      origin: new google.maps.Point(...model.icon.origin),
      anchor: new google.maps.Point(...model.icon.anchor),
    },
  });

  marker.setMap(_map);
  google.maps.event.addListener(marker, 'click', () => markerSelectedEvent(model));

  return marker;
}

function _removeMarker(marker) {
  marker.setMap(null);
}

function _updateMarker(marker, model) {
  var position = new google.maps.LatLng(...model.position);
  var color = model.selected ? 'red' : 'yellow' || 'yellow';

  if (position !== marker.getPosition()) {
    marker.setPosition(new google.maps.LatLng(...model.position));
    marker.setIcon({
      url   : `img/aircraft-${color}-icons.png`,
      size  : new google.maps.Size(...model.icon.size),
      origin: new google.maps.Point(...model.icon.origin),
      anchor: new google.maps.Point(...model.icon.anchor),
    });
  }
}

function _addPolyline(model) {
  var polyline = new google.maps.Polyline({
      path         : model.path.map(node => new google.maps.LatLng(node[1], node[0])),
      geodesic     : true,
      strokeColor  : '#cb3deb',
      strokeOpacity: 1.0,
      strokeWeight : 2
  });

  polyline.setMap(_map);

  return polyline;
}

function _removePolyline(polyline) {
  if (polyline) {
    polyline.setMap(null);
  }
}
