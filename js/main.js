var _data;

/**
 * DATA functions
 */
function pool(url, onload) {
  const recurse = () => pool(url, onload);
  let req = new XMLHttpRequest();

  req.open('GET', url, true);
  req.onload = function() {
    onload(JSON.parse(req.responseText));
    setTimeout(recurse, 60000);
  };

  req.send();
}

function arrayToObject(key) {
  return (obj, item) => {return {...obj, [item[key]]: item}};
}

/**
 * PRESENTATION functions
 */
function getContext() {
  return {
    data: Array(),
    selectedPilots: new Set(),
    markers: Array(),
    polylines: Array(),
  };
}

function vatsimStateChanged(context, data) {
  var markers = getMarkers(context, data);
  var polylines = getPolylines(context, data);
  _data = data;

  return {markers: markers, polylines: polylines};
}

function pilotSelected(context, model) {
  if (context.selectedPilots.has(model._id)) {
    context.selectedPilots.delete(model._id);
  } else {
    context.selectedPilots.clear();
    context.selectedPilots.add(model._id);
  }

  return vatsimStateChanged(context, _data);
}

function getMarkers(context, data) {
  return data['_items']
    .filter(e => e.clienttype === 'PILOT')
    .map((e) => pilotToMarker(e, context.selectedPilots.has(e._id) ? 'red' : 'yellow'));
}

function getPolylines(context, data) {
  return data['_items']
    .filter(e => e.clienttype === 'PILOT' && context.selectedPilots.has(e._id))
    .map(pilotToPolyline);
}

/**
 * MAPPING functions
 */
function pilotToMarker(pilot, color) {

  function offsetForHeading(heading) {
    var normalized = (heading > 180) ? heading - 180 : heading + 180;
    return Math.round(Math.abs(normalized / 15));
  }

  var xOffset = offsetForHeading(parseInt(pilot.heading));

  return {
    _id: pilot._id,
    position: [pilot.location[1], pilot.location[0]],
    title   : `${pilot.callsign}\t${pilot.planned_aircraft}\t${pilot.planned_depairport} - ${pilot.planned_destairport}\n${pilot.altitude}ft\t${pilot.groundspeed}kts\n${pilot.realname} (${pilot.cid})`,
    icon    : {
      url   : `img/aircraft-${color}-icons.png`,
      size  : [23, 23],
      origin: [xOffset * 48, 3 * 48],
      anchor: [11.5, 11.5],
    },
  };
}

function pilotToPolyline(pilot) {
  return {
    path: pilot.location_history.coordinates.map((coordinate => [coordinate[0], coordinate[1]])),
  };
}
